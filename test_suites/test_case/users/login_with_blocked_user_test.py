__author__ = 'dsafinski'

import unittest
import test_suites
from holmium.core import TestCase
from test_suites.pages.login_page import LoginPage


class LoginTest(TestCase):
    def setUp(self):
        self.login_page = test_suites.pages.login_page.LoginPage(self.driver, self.config['base_url'])

    def test_blocked_user(self):
        self.login_page.login('rozalia.adamska@example.com', 'admin')
        assert 'Twoje konto jest zablokowane.' in self.driver.page_source

if __name__ == "__main__":
    unittest.main()
