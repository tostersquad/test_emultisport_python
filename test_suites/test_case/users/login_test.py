__author__ = 'dsafinski'
# -*- coding: utf-8 -*-
import unittest
import test_suites
from test_suites.pages.login_page import LoginPage
from holmium.core import TestCase


class LoginTest(TestCase):
    def setUp(self):
        self.login_page = test_suites.pages.login_page.LoginPage(self.driver, self.config['base_url'])

    def test_login_with_good_credential(self):
        self.login_page.login('ewa.baranowska@example.org', 'admin')
        assert 'Zalogowano jako: ewa.baranowska@example.org' in self.driver.page_source

    def test_login_with_wrong_username(self):
        self.login_page.login('ewa.baranowska@example.orgtest', 'admin')
        # assert 'Nieprawidłowy e-mail lub hasło' in self.driver.page_source

    def test_login_with_wrong_password(self):
        self.login_page.login('ewa.baranowska@example.org', 'test')
        # assert 'Nieprawidłowy e-mail lub hasło' in self.driver.page_source

    def test_login_with_no_credential(self):
        self.login_page.login('', '')


if __name__ == "__main__":
    unittest.main()
