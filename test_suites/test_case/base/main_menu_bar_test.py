__author__ = 'dsafinski'
# -*- coding: utf-8 -*-
import unittest
from test_suites.pages.login_page import LoginPage
from holmium.core import TestCase
from test_suites.pages.navigation_page import NavigationPage
import test_suites


class MainMenuBar(TestCase):
    def setUp(self):
        self.login_page = test_suites.pages.login_page.LoginPage(self.driver, self.config['base_url'])
        self.nav = test_suites.pages.navigation_page.NavigationPage(self.driver)

    def test_click_menu(self):
        self.login_page.login('ewa.baranowska@example.org', 'admin')

        self.nav.history_page()
        assert 'Historia' in self.driver.page_source

        self.nav.messages_page()
        # assert 'Wiadomości' in self.driver.page_source

        self.nav.sport_object_page()
        assert 'Obiekty sportowe' in self.driver.page_source

        self.nav.help_page()
        assert 'Pomoc' in self.driver.page_source

        self.nav.start_tour_page()
        assert 'Przewodnik' in self.driver.page_source

        self.nav.start_page()
        assert 'Start' in self.driver.page_source

if __name__ == "__main__":
    unittest.main()
