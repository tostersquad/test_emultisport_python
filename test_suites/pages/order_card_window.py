__author__ = 'mikolaj'

from holmium.core import Page, Element, Locators, ElementMap


class OrderCardWindow(Page):

    FIRST_NAME_ID = 'new_card_firstname'  # Trzy zmienne przechowują ID textboxów Imię i Nazwisko przy dodawaniu nowej karty oraz przycisku "Dodaj"
    LAST_NAME_ID = 'new_card_lastname'
    SUBMIT_ID = 'addEditCardSubmit'
    first_name_box = Element(Locators.ID, FIRST_NAME_ID)
    last_name_box = Element(Locators.ID, LAST_NAME_ID)
    submit_button = Element(Locators.ID, SUBMIT_ID)
    select_picker = Element(Locators.XPATH, "//button[@class='btn dropdown-toggle selectpicker']")
    order_card_data = ElementMap(Locators.CLASS_NAME, "text")

    '''
    Funkcja wybiera typ karty z rozwijanej listy w oknie dodawania nowej karty. Typ karty należy podać jako string i musi wystąpić w
    pełnej formie, tak jak na stronie czyli, typ + cena (dla przypadku, w którym ten sam typ karty występuje w dwóch cenach)
    '''

    def choose_cards_type(self, type):
        self.select_picker.click()
        self.driver.implicitly_wait(0.5)
        type_button = self.driver.find_element_by_link_text(type)
        type_button.click()

    '''
    Funkcja przyjmuje jako argument imię osoby, dla której ma zostać dodana nowa karta i wpisuje w odpowiedni textbox
    '''

    def write_first_name(self, first_name):
        self.first_name_box.send_keys(first_name)

    '''
    Funkcja przyjmuje jako argument nazwisko osoby, dla której ma zostać dodana nowa karta i wpisuje w odpowiedni textbox
    '''

    def write_last_name(self, last_name):
        self.last_name_box.send_keys(last_name)

    '''
    Funkcja klika w przycisk zatwierdzający dodanie nowej karty
    '''

    def submit_new_card(self):
        self.submit_button.click()

    def get_card_types(self):
        self.select_picker.click()
        self.driver.implicitly_wait(0.5)
        types = []
        for typ in self.order_card_data:
            if typ[-7:] == 'limitem':
                types.append(typ[:-12])
            else:
                types.append(typ)
        print(types)
        return types

    def get_available_card_types(self):
        self.select_picker.click()
        self.driver.implicitly_wait(0.5)
        available_types = []
        for typ in self.order_card_data:
            if typ[-7:] != 'limitem':
                available_types.append(typ)
        return available_types

    def get_unavailable_card_types(self):
        self.select_picker.click()
        self.driver.implicitly_wait(0.5)
        unavailable_types = []
        for typ in self.order_card_data:
            if typ[-7:] == 'limitem':
                unavailable_types.append(typ[:-12])
        return unavailable_types
