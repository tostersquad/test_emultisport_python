__author__ = 'mikolaj'

from holmium.core import Page, Element, Locators, ElementMap
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import re
from holmium.core.facets import FacetError


class UserDashboardStart(Page):

    add_card_button = Element(Locators.ID, "addCardButton")
    order_button = Element(Locators.XPATH, "//a[@class='btn light btn-primary btn-lg btn-block pay']")

    '''
    Funkcja pobiera ze strony wiersze tabeli i zwraca je jako obiekt typu WebElement
    '''

    def get_table_rows(self):
        table_rows = self.driver.find_elements(By.TAG_NAME, "tr")
        return table_rows

    '''
    Funkcja jako argument przyjmuje wiersze tabeli jako obiekty typu WebElement. Następnie każdą komórkę tabeli
    konwertuje do stringa i dołącza do zainicjowanej na początku listy data. Funkcja zwraca tę listę.
    '''

    def get_table_data(self, table_rows):
        data = []
        for tr in table_rows:
            table_data = tr.find_elements(By.TAG_NAME, "td")
            if table_data:
                for td in table_data:
                    data.append(td.text)
        return data

    '''
    Funkcja pomocnicza dla poniższej (find_word_in_source). Korzysta z wyrażeń regularnych, aby ustalić, czy słowo
    znajduje się w źródle w dokładnie takiej postaci, w jakiej został zadany.
    '''

    def find_whole_word(self, word):
        return re.compile(r'\b({0})\b'.format(word), flags=re.IGNORECASE).search

    '''
    Funkcja przyjmuje dwa argumenty: word- string i source- bufor. Wyszukuje, czy string znajduje się w zadanym buforze,
    w dokładnie takiej postaci, w jakiej został zadany. W zależności od tego zwraca True/False.
    '''

    def find_word_in_source(self, word, source):
        if type(source) == list:
            source = ' '.join(source)
        found = self.find_whole_word(word)(source)
        try:
            assert found != None
        except AssertionError:
            return False
        return True

    '''
    Funkcja sprawdza, czy karta dla posiadacza o zadanym nazwisku i imieniu, bądź numerze karty istnieje. W zależności
    od tego, zwraca True/False
    '''

    def check_if_card_exists(self, name):
        self.driver.implicitly_wait(0.5)
        rows = self.get_table_rows()
        data = self.get_table_data(rows)
        return self.find_word_in_source(name, data)

    '''
    Funkcja zwraca status ("AKTYWNA", "NIEAKTYWNA", "NOWA KARTA") karty dla posiadacza o zadanym nazwisku i imieniu, bądź
    numerze karty.
    '''

    def check_cards_status(self, name):
        self.driver.implicitly_wait(0.5)
        rows = self.get_table_rows()
        data = self.get_table_data(rows)
        if self.find_word_in_source(name, data):
            for index, string in enumerate(data):
                if name in string:
                    status = data[index + 1]
                    '''W zmiennej status przechowywany jest status danej karty zaciągnięty z tabeli ze strony. W tych
                    samych komórkach trzymane są też stringi "Opcje" bądź "Przenieść do archiwum?" w zależności od statusu.
                    Poniższa konstrukcja wyłuskuje i zwraca wyłącznie stringi: "AKTYWNA", "NIEAKTYWNA" lub "NOWA KARTA".'''
                    if status[-5:] == 'Opcje':
                        return status[:-6]
                    elif status[-9:] == 'archiwum?':
                        return status[:-25]
                    else:
                        return status

    '''
    Funkcja sprawdza, czy aktywny (klikalny) jest przycisk dodania nowej karty i w zależności od tego zwraca True/False
    '''

    def is_add_card_active(self):
        try:
            self.driver.find_element_by_xpath("//button[@class='btn btn-lg btn-block add']")
        except NoSuchElementException:
            return False
        return True

    '''
    Funkcja klika w przycisk dodania nowej karty
    '''

    def click_add_card(self):

        self.add_card_button.click()

    '''
    Funkcja sprawdza, czy aktywny (klikalny) jest przycisk przejścia do podsumowania zamówienia i w zależności od tego
    zwraca True/False.
    '''

    def is_order_active(self):
        try:
            self.driver.find_element_by_xpath("//a[@class='btn light btn-primary btn-lg btn-block pay']")
        except NoSuchElementException:
            return False
        return True

    '''
    Funkcja klika w przycisk przejścia do podsumowania zamówienia
    '''

    def click_order(self):
        self.order_button.click()
