__author__ = 'dsafinski'

from holmium.core import Page, Element, Locators


class NavigationPage(Page):
    start = Element(Locators.XPATH, ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/a")
    history = Element(Locators.XPATH, ".//*[@id='bs-example-navbar-collapse-1']/ul/li[2]/a")
    messages = Element(Locators.XPATH, ".//*[@id='bs-example-navbar-collapse-1']/ul/li[3]/a")
    sports_object = Element(Locators.XPATH, ".//*[@id='bs-example-navbar-collapse-1']/ul/li[4]/a")
    help = Element(Locators.XPATH, ".//*[@id='bs-example-navbar-collapse-1']/ul/li[5]/a")
    start_tour = Element(Locators.XPATH, ".//*[@id='bs-example-navbar-collapse-1']/ul/li[6]/a")

    def start_page(self):
        self.start.click()

    def history_page(self):
        self.history.click()

    def messages_page(self):
        self.messages.click()

    def sport_object_page(self):
        self.sports_object.click()

    def help_page(self):
        self.help.click()

    def start_tour_page(self):
        self.start_tour.click()