__author__ = 'mikolaj'

from holmium.core import Page, Element, Locators, ElementMap
from selenium.webdriver.common.by import By


class History(Page):

    '''
    Funkcja pobiera ze strony wiersze tabeli i zwraca je jako obiekt typu WebElement
    '''
    def get_table_rows(self):
        table_rows = self.driver.find_elements(By.TAG_NAME, "tr")
        return table_rows

    '''
    Funkcja jako argument przyjmuje wiersze tabeli jako obiekty typu WebElement. Następnie każdą komórkę tabeli
    konwertuje do stringa i dołącza do zainicjowanej na początku listy data. Funkcja zwraca tę listę.
    '''
    def get_table_data(self, table_rows):
        data = []
        for tr in table_rows:
            table_data = tr.find_elements(By.TAG_NAME, "td")
            if table_data:
                for td in table_data:
                    data.append(td.text)
        return data

    '''
    Poniższe funkcje zwracają dane dotyczące ostatnio dokonanej operacji. Wyłuskują te dane z tabeli przechowywanej na
    stronie Historii za pomocą dwóch powyższych funkcji
    '''

    #Funkcja zwraca nazwisko posiadacza karty, której dotyczy ostatnio dokonana operacja
    def get_last_operations_name(self):
        data = self.get_table_data(self.get_table_rows())
        return data[2]

    #Funkcja zwraca typ ostatnio dokonanej operacji
    def get_last_operations_type(self):
        data = self.get_table_data(self.get_table_rows())
        return data[3]

    #Funkcja zwraca status ostatnio dokonanej operacji
    def get_last_operations_status(self):
        data = self.get_table_data(self.get_table_rows())
        return data[5]

    #Funkcja zwraca adres, jeśli ostatnio dokonaną operacją było dodanie nowej karty
    #TO DO!!!
    def get_last_operations_address(self):
        pass
